# WeTravel WebAPI #

This project is a academic project for HKU ECom-IComp course **ICOM6034 Website Engineering 2016 (Semester1)**

### What is WeTravel? ###

WeTravel is a website designed for a group of friends to share travel ideas and their past travel experiences, and comments/thoughts within their social circle. Please visit https://wetravelwebapp.azurewebsites.net to enter the website.


### Overview ###

A user can post a new travel idea with the planned destination and schedule. S/he can attach tags to his/her idea and others can search the ideas by using those tags as keywords. The tags are also the keywords that can be used for interacting with other web APIs through service mash-ups in the website. The mashups can help provide further information about the user’s planned trips; examples of such information include but are not limited to hotel/flight information, local special events or festivals, catering services, etc. For simplicity, we assume that all registered users belong to the same “group of friends”, and that this website supports a single group of friends only.

### This project is owned by Group C (DoubleJ) ###

* Joe Lau (3035156872), joecklau@connect.hku.hk
* Jack Yuen (3035246603), u3524660@connect.hku.hk