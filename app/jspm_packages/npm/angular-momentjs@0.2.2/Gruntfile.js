/* */ 
'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function(connect, dir) {
  return connect.static(require('path').resolve(dir));
};
module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bwr: grunt.file.readJSON('bower.json'),
    concat: {dist: {}},
    ngAnnotate: {dist: {}},
    uglify: {
      options: {
        report: 'min',
        enclose: {
          'this': 'window',
          'this.angular': 'angular',
          'void 0': 'undefined'
        },
        banner: '/*\n  <%= pkg.name %> - v<%= pkg.version %> \n  ' + '<%= grunt.template.today("yyyy-mm-dd") %>\n*/\n' + ''
      },
      dist: {
        options: {
          beautify: false,
          mangle: true,
          compress: {
            global_defs: {'DEBUG': false},
            dead_code: true
          },
          sourceMap: '<%= bwr.name %>.min.js.map'
        },
        files: {'<%= bwr.name %>.min.js': ['./lib/index.js', './lib/*/*.js']}
      },
      src: {
        options: {
          beautify: true,
          mangle: false,
          compress: false
        },
        files: {'<%= bwr.name %>.js': ['./lib/index.js', './lib/*/*.js']}
      }
    },
    watch: {livereload: {
        options: {livereload: LIVERELOAD_PORT},
        files: ['example/{,*/}*.html', 'example/{,*/}*.js', '{,*/}*.js']
      }},
    connect: {
      options: {
        port: '3000',
        hostname: 'localhost'
      },
      livereload: {options: {middleware: function(connect) {
            return [lrSnippet, mountFolder(connect, '.tmp'), mountFolder(connect, 'example'), mountFolder(connect, '.')];
          }}}
    },
    jshint: {
      options: {jshintrc: '.jshintrc'},
      gruntfile: {src: 'Gruntfile.js'},
      lib: {src: ['lib/**/*.js']}
    },
    complexity: {generic: {
        src: ['lib/**/*.js'],
        options: {
          jsLintXML: 'report.xml',
          checkstyleXML: 'checkstyle.xml',
          errorsOnly: false,
          cyclomatic: 3,
          halstead: 8,
          maintainability: 100
        }
      }}
  });
  grunt.registerTask('server', function(target) {
    grunt.task.run(['connect:livereload', 'watch']);
  });
  grunt.registerTask('test', ['jshint']);
  grunt.registerTask('build', ['concat', 'ngAnnotate', 'uglify']);
  grunt.registerTask('default', ['build']);
};
