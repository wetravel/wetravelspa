System.config({
  defaultJSExtensions: true,
  transpiler: "traceur",
  paths: {
    "app/*": "src/*.js",
    "users/*": "src/users/*.js",
    "material-start/*": "src/*.js",
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*"
  },

  map: {
    "angular": "github:angular/bower-angular@1.5.9",
    "angular-animate": "github:angular/bower-angular-animate@1.5.9",
    "angular-aria": "github:angular/bower-angular-aria@1.5.9",
    "angular-cookies": "github:angular/bower-angular-cookies@1.5.9",
    "angular-google-maps": "npm:angular-google-maps@2.4.0",
    "angular-material": "github:angular/bower-material@master",
    "angular-messages": "github:angular/bower-angular-messages@1.5.9",
    "angular-mocks": "github:angular/bower-angular-mocks@1.5.8",
    "angular-moment": "npm:angular-moment@1.0.1",
    "angular-momentjs": "npm:angular-momentjs@0.2.2",
    "angular-route": "github:angular/bower-angular-route@1.5.9",
    "angular-signalr-hub": "npm:angular-signalr-hub@1.6.2",
    "angular-simple-logger": "npm:angular-simple-logger@0.1.7",
    "angular-ui-tinymce": "npm:angular-ui-tinymce@0.0.17",
    "css": "github:systemjs/plugin-css@0.1.32",
    "jquery": "npm:jquery@3.1.1",
    "json": "github:systemjs/plugin-json@0.1.2",
    "lodash": "npm:lodash@4.17.2",
    "moment": "npm:moment@2.17.0",
    "signalr": "npm:signalr@2.2.1",
    "text": "github:systemjs/plugin-text@0.0.4",
    "tinymce": "npm:tinymce@4.5.0",
    "traceur": "github:jmcriffey/bower-traceur@0.0.93",
    "traceur-runtime": "github:jmcriffey/bower-traceur-runtime@0.0.93",
    "github:angular/bower-angular-animate@1.5.9": {
      "angular": "github:angular/bower-angular@1.5.9"
    },
    "github:angular/bower-angular-aria@1.5.9": {
      "angular": "github:angular/bower-angular@1.5.9"
    },
    "github:angular/bower-angular-cookies@1.5.9": {
      "angular": "github:angular/bower-angular@1.5.9"
    },
    "github:angular/bower-angular-messages@1.5.9": {
      "angular": "github:angular/bower-angular@1.5.9"
    },
    "github:angular/bower-angular-route@1.5.9": {
      "angular": "github:angular/bower-angular@1.5.9"
    },
    "github:angular/bower-material@master": {
      "angular": "github:angular/bower-angular@1.5.9",
      "angular-animate": "github:angular/bower-angular-animate@1.5.9",
      "angular-aria": "github:angular/bower-angular-aria@1.5.9",
      "angular-messages": "github:angular/bower-angular-messages@1.5.9",
      "css": "github:systemjs/plugin-css@0.1.32"
    },
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.4.1"
    },
    "github:jspm/nodelibs-buffer@0.1.0": {
      "buffer": "npm:buffer@3.6.0"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.9"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "github:jspm/nodelibs-vm@0.1.0": {
      "vm-browserify": "npm:vm-browserify@0.0.4"
    },
    "npm:angular-google-maps@2.4.0": {
      "js-rich-marker": "github:pocesar/js-rich-marker@1.0.0"
    },
    "npm:angular-moment@1.0.1": {
      "moment": "npm:moment@2.17.0"
    },
    "npm:angular-momentjs@0.2.2": {
      "angular": "npm:angular@1.5.9",
      "moment": "npm:moment@2.17.0",
      "path": "github:jspm/nodelibs-path@0.1.0"
    },
    "npm:angular-simple-logger@0.1.7": {
      "angular": "npm:angular@1.5.9",
      "debug": "npm:debug@2.3.3"
    },
    "npm:angular-ui-tinymce@0.0.17": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:assert@1.4.1": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "util": "npm:util@0.10.3"
    },
    "npm:buffer@3.6.0": {
      "base64-js": "npm:base64-js@0.0.8",
      "child_process": "github:jspm/nodelibs-child_process@0.1.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "ieee754": "npm:ieee754@1.1.8",
      "isarray": "npm:isarray@1.0.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:debug@2.3.3": {
      "ms": "npm:ms@0.7.2"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.9": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "vm": "github:jspm/nodelibs-vm@0.1.0"
    },
    "npm:signalr@2.2.1": {
      "jquery": "npm:jquery@3.1.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:tinymce@4.5.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:vm-browserify@0.0.4": {
      "indexof": "npm:indexof@0.0.1"
    }
  }
});
