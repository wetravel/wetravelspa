export default {
    name : 'logout',
    config : {
        parent: angular.element(document. getElementById('content')),
        templateUrl         : 'src/accounts/components/logout/Logout.html',
        controllerAs        : '$ctrl',
        bindToController    : true,
        disableBackdrop     : true,
        disableParentScroll : false,
        controller          : [ '$rootScope', '$mdBottomSheet', '$mdToast', '$log', '$scope', '$http', '$cookies', '$window', class LogoutController {

            /**
             * Constructor
             */
            constructor($rootScope, $mdBottomSheet, $mdToast, $log, $scope, $http, $cookies, $window) {
                this.$rootScope = $rootScope;
                this.$mdBottomSheet = $mdBottomSheet;
                this.$mdToast = $mdToast;
                this.$log = $log;
                this.$scope = $scope;
                this.$http = $http;
                this.$cookies = $cookies;
                this.$window = $window;
            }

            /**
             * Logout
             */
            logout() {
                var self = this;
                self.$rootScope.progress = true;

                self.$http({
                    method: 'POST',
                    url: self.$rootScope.baseUrl + 'api/Account/Logout',
                    headers: {'Authorization': 'bearer ' + self.$cookies.get('token')}
                })
                .success(function (data) {
                    self.$cookies.remove('username');
                    self.$cookies.remove('token');
                    self.$rootScope.progress = false;
                    self.$mdToast.showSimple('Logged out successfully.');
                    self.$window.location.href = '#login';
                })
                .error(function (data, status) {
                    self.$cookies.remove('username');
                    self.$cookies.remove('token');
                    if (data.Message) {
                        self.$mdToast.showSimple(data.Message);
                    }
                    self.$rootScope.progress = false;
                    self.$window.location.href = '#list-travel-ideas';
                });
            }
        } ]
    }
};


