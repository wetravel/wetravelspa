export default {
    name : 'listTravelIdea',
    config : {
        parent: angular.element(document. getElementById('content')),
        templateUrl      : 'src/travel-ideas/components/list/ListTravelIdeas.html',
        controllerAs     : '$ctrl',
        bindToController : true,
        disableBackdrop  : true,
        disableParentScroll : false,
        controller       : [ '$rootScope', '$mdBottomSheet', '$mdToast', '$log', '$scope', '$http', '$cookies', '$httpParamSerializerJQLike', '$sce', class ListTravelIdeasController {

            /**
             * Constructor
             */
            constructor($rootScope, $mdBottomSheet, $mdToast, $log, $scope, $http, $cookies, $httpParamSerializerJQLike, $sce) {
                this.$rootScope = $rootScope;
                this.$mdBottomSheet = $mdBottomSheet;
                this.$mdToast = $mdToast;
                this.$log = $log;
                this.$scope = $scope;
                this.$http = $http;
                this.$cookies = $cookies;
                this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
                this.$sce = $sce;
                this.ideas = [];

                this.searchedTags = [];
                this.searchedKeywords = '';
                this.searchCountText = 'Loading...';
            }

            /**
             * Load Travel Ideas
             */
            loadIdeas(isTextOnly, newItemsAfter, tags, keywords) {
                var self = this;
                var params = {};
                if (isTextOnly) {
                    params.isTextOnly = true;
                }
                if (newItemsAfter) {
                    params.newItemsAfter = newItemsAfter;
                }
                if (tags) {
                    params.tags = tags;
                }
                if (keywords) {
                    params.keywords = keywords;
                }
                self.$rootScope.progress = true;

                self.$http({
                    method: 'GET',
                    url: self.$rootScope.baseUrl + 'api/TravelIdea',
                    headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                    params: params,
                    paramSerializer: '$httpParamSerializerJQLike'
                })
                .success(function (data) {
                    self.ideas = data.Payload;
                    self.ideas.forEach(function (item) {
                        item.Content = self.$sce.trustAsHtml(item.ContentRTF ? item.ContentRTF : item.ContentText);
                    });
                    self.searchCount = self.ideas.length;
                    if (self.ideas.length < 1) {
                        self.searchCountText = 'No travel ideas were found.';
                    } else if (self.ideas.length == 1) {
                        self.searchCountText = '1 travel idea was found.';
                    } else {
                        self.searchCountText = self.ideas.length + ' travel ideas were found.';
                    }
                    self.$rootScope.progress = false;
                })
                .error(function (data, status) {
                    if (data.Message) {
                        self.$mdToast.showSimple(data.Message);
                    }
                    self.$rootScope.progress = false;
                });
            }

            deleteIdea(id) {
                var self = this;
                self.$rootScope.progress = true;

                self.$http({
                    method: 'DELETE',
                    url: self.$rootScope.baseUrl + 'api/TravelIdea',
                    headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                    params: {id: id},
                    paramSerializer: '$httpParamSerializerJQLike'
                })
                .success(function (data) {
                    self.ideas.forEach(function (item, index) {
                        if (item.Id === id) {
                            self.ideas.splice(index, 1);
                        }
                    });
                    self.$rootScope.progress = false;
                    self.$mdToast.showSimple('Delete a travel idea successfully.');
                })
                .error(function (data, status) {
                    if (data.Message) {
                        self.$mdToast.showSimple(data.Message);
                    }
                    self.$rootScope.progress = false;
                });
            }

            getUserName() {
                return this.$cookies.get('username');
            }

            selectTag(tag) {
                if (!this.searchedTags.includes(tag)) {
                    this.searchedTags.push(tag);
                    this.loadIdeas(false, null, this.searchedTags);
                }
            }
        } ]
    }
};


